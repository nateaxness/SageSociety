import React from "react"; // added because it's required for JSX
import { useState } from "react";
import { AuthProvider } from "@galvanize-inc/jwtdown-for-react";
import { AccountContext } from "./AccountContext";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import "./App.css";
import Nav from "./Nav";
import GardensList from "./Gardens/GardensList";
import PlantsPage from "./Plants/PlantsPage.js";
import SignupForm from "./Accounts/SignupForm";
import LoginForm from "./Accounts/LoginForm";
import MainPage from "./Main";
import Groups from "./Groups";
import LogoutButton from "./Accounts/Logout";
import AddGardenForm from "./Gardens/AddGardenForm";
import EditGardenForm from "./Gardens/EditGardenForm";
import CreateBlogForm from "./Blog/CreateBlogForm.js";
import BlogDetail from "./Blog/BlogDetail";
import CreateBlogPost from "./BlogPost/CreateBlogPost";
import PostList from "./BlogPost/BlogPostList";
import PostDetail from "./BlogPost/BlogPostDetail";

function App() {
  const domain = /https:\/\/[^/]+/;
  const basename = process.env.PUBLIC_URL.replace(domain, "");
  const [accountData, setAccountData] = useState(null);
  const [selectedZipCode, setSelectedZipCode] = useState(null);

  return (
    <AccountContext.Provider value={{ accountData, setAccountData }}>
      <BrowserRouter basename={basename}>
        <AuthProvider baseUrl={process.env.REACT_APP_API_HOST}>
          <div>
            <Nav />
            <Routes>
              <Route
                path="/gardens"
                element={
                  <GardensList
                    element={GardensList}
                    setSelectedZipCode={setSelectedZipCode}
                  />
                }
              />
              <Route
                path="/gardens/:gardens_id/plants"
                element={<PlantsPage zipCode={selectedZipCode} />}
              />
              <Route path="/gardens" element={<GardensList />} />
              <Route
                path="/gardens/:gardens_id/edit"
                element={<EditGardenForm />}
              />
              <Route
                path="/gardens/:gardens_id/plants"
                element={<PlantsPage />}
              />
              <Route path="/gardens/add" element={<AddGardenForm />} />
              <Route path="/" element={<MainPage />} />
              <Route path="/signup" element={<SignupForm />} />
              <Route path="/login" element={<LoginForm />} />
              <Route path="/logout" element={<LogoutButton />} />
              <Route path="/groups" element={<Groups />} />
              <Route path="/nav" element={<Nav />} />
              <Route path= "/blog" element={<CreateBlogForm/>} />
              <Route path= "/blog/:blogs_id" element={<BlogDetail/>} />
              <Route path= "/blog/:blogs_id/blogpost" element={<CreateBlogPost/>} />
              <Route path= "/blog/:blogs_id/blogpost/get" element={<PostList/>}/>
              <Route path= "/blog/:blogs_id/blogpost/:blogs_post_id" element={<PostDetail/>}/>

          </Routes>
          </div>
        </AuthProvider>
      </BrowserRouter>
    </AccountContext.Provider>
  );
}

export default App;
