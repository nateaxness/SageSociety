import React, {useState} from "react";
import {useParams,useNavigate} from "react-router-dom";

export default function CreateBlogPost(){
const[title,setTitle]= useState("");
const[picture_url,setPicture_URL]= useState("");
const[text,setText]= useState("");
const {blogs_id}=useParams();

const navigate= useNavigate();

const HandleSubmit = async (e) => {

    e.preventDefault();

    const response = await fetch(`${process.env.REACT_APP_API_HOST}/blog/+${blogs_id}/blogpost`, {
      method: "POST",
      credentials: "include",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        blogs_id:blogs_id,
        title:title,
        picture_url:picture_url,
        text:text,
      }),
    });
     if (!response.ok) {
        const errorData = await response.json();
        throw new Error(`Server responded with ${response.status}: ${errorData.detail}`);
      }
    const data = await response.json();
    const linkblogsid = data.blogs_id;

    navigate(`/blog/${linkblogsid}/blogpost/get`);

  }


return(
<>
 <div className="min-h-screen flex items-center justify-center bg-gray-100">
        <div className="bg-white p-8 rounded-lg shadow-md w-96">
            <h1 className="text-2xl mb-6 text-center font-bold">
            Create a Post </h1>
            <form onSubmit={HandleSubmit}>
              <div className="form-floating mb-3">
               <label  className="block text-lg font-bold mb-2" htmlFor="Blog Title">Title</label>
                <input
                placeholder="Post Title"
                type="text"
                name="Post Title"
                id="Post Title"
                className="shadow appearance-none border rounded w-full py-2 px-3 leading-tight focus:outline-none focus:shadow-outline"
                value={title}
                onChange={(e) => setTitle(e.target.value)}
                required
                />


                </div>
                 <div className="form-floating mb-3">
                  <label className="block text-lg font-bold mb-2"  htmlFor="Picture_Url">Image</label>
                <input
                placeholder="Picture Url"
                type="text"
                name="Picture Url"
                id="Picture Url"
                className="shadow appearance-none border rounded w-full py-2 px-3 leading-tight focus:outline-none focus:shadow-outline"
                value={picture_url}
                onChange={(e) => setPicture_URL(e.target.value)}
                required
                />
                </div>

                 <div className="form-floating mb-3">
                <label className="block text-lg font-bold mb-2"  htmlFor="Text">Content</label>
                <input
                placeholder="Text"
                type="text"
                name="Text"
                id="Text"
                className="shadow appearance-none border rounded w-full py-2 px-3 leading-tight focus:outline-none focus:shadow-outline"
                value={text}
                onChange={(e) => setText(e.target.value)}
                required
                />

                </div>
              <button type="submit" className="w-full bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">
                Submit
              </button>
            </form>
          </div>
        </div>
    </>
)
}
