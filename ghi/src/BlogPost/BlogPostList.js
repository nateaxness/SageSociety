
import React, { useState ,useEffect} from "react";
import { Link } from "react-router-dom";
import { useParams} from "react-router-dom";

export default function PostList(){
    const[blogposts,setBlogPosts]= useState([])
    const{blogs_id}=useParams();


    const fetchAllPosts= async()=>{
        const url= `${process.env.REACT_APP_API_HOST}/blog/${blogs_id}/blogpost`;
        const response = await fetch(url,
            {credentials:"include",
        });
        if(response.ok){
            const data = await response.json();
            setBlogPosts(data.blogposts);
        }
    };

    useEffect(()=>{
        fetchAllPosts();
        //eslint-disable-next-line react-hooks/exhaustive-deps
    },[blogs_id]);




    return (
     <div className="min-h-screen flex items-center justify-center bg-gray-100">
      <div className="bg-white p-8 rounded-lg shadow-md w-full max-w-2xl">
       <div>
         <h1>My Posts</h1>
           {/* find out how to dynamically place blog title above */}
       <ul>
        {blogposts.map((blogpost) => (

          <li key={blogpost.blogs_post_id} className="mb-4 bg-gray-100 p-4 rounded shadow">
            <h2><Link to={`/blog/${blogs_id}/blogpost/${blogpost.blogs_post_id}`}>{blogpost.title}</Link></h2>
            <img
              className="w-24 h-24"
              src={blogpost.picture_url}
              alt="Post Pic"
            />
            <p className="mb-2">{blogpost.text}</p>
          </li>

        ))}
      </ul>
      </div>
     <button type="submit" className="w-full bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">
     <Link to ={`/blog/${blogs_id}/blogpost/`}>  Create New Post</Link>
     </button>
    </div>
    </div>
  );




};
