import React from "react";
import { NavLink } from "react-router-dom";
import LogoutButton from "./Accounts/Logout";

function Nav() {
  return (
    <nav className="bg-green-600 p-4 text-white">
      <div className="container mx-auto">
        <div className="flex justify-between items-center">
          <NavLink className="text-2xl font-bold" to="/">
            SageSociety
          </NavLink>
          <div className="space-x-4">
            <NavLink
              className="hover:bg-green-500 p-2 rounded"
              activeclassname="underline"
              to="/gardens"
            >
              My Gardens
            </NavLink>
            <NavLink
              className="hover:bg-green-500 p-2 rounded"
              activeclassname="underline"
              to="/groups"
            >
              Groups
            </NavLink>
            <NavLink
              className="hover:bg-green-500 p-2 rounded"
              activeclassname="underline"
              to="/blog"
            >
              Create Blog
            </NavLink>
            <NavLink
              className="hover:bg-green-500 p-2 rounded"
              activeclassname="underline"
              to="/signup"
            >
              Signup
            </NavLink>
             <NavLink
              className="hover:bg-green-500 p-2 rounded"
              activeclassname="underline"
              to="/login"
            >
              Login
            </NavLink>

            <LogoutButton />



          </div>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
