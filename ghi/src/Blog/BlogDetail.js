import React from "react";
import {useParams,Link} from "react-router-dom";
import { useState,useEffect } from "react";


export default function BlogDetail(){
const{blogs_id}= useParams();
const[blog, setBlog]=useState();

const fetchBlogDetail= async()=>{
    const url =`${process.env.REACT_APP_API_HOST}/blog/${blogs_id}`;
    const response= await fetch(url,
         {credentials: "include",
      });

    if(response.ok){
        const data= await response.json();
        setBlog(data);
    }
};
useEffect(()=>{
    fetchBlogDetail();
//eslint-disable-next-line react-hooks/exhaustive-deps
},[blogs_id]);


return(
<>
{blog? (
        <div>
            <h1 className="text-2xl mb-6 text-center mt-4 font-bold">{blog.title}</h1>
        </div>
      ) : null}
<button type="submit"
 className="w-24px  mt-4  content-center bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">
    <Link to={`/blog/${blogs_id}/blogpost`}>
    Create a Post
    </Link>
</button>



</>
);
}
