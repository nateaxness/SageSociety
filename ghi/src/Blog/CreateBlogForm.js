import React, {useState} from "react";
import { useNavigate } from "react-router-dom";
import useToken from "@galvanize-inc/jwtdown-for-react";


export default function CreateBlogForm(){

  const[title, setTitle]= useState("");
  const navigate= useNavigate();
  let createdblogid;

  const handleChangeTitle=(event)=>{
    const value=event.target.value;
    setTitle(value);
  };

  const { token } = useToken();

  const getAccountIdFromToken = () => {
  const decodedToken = JSON.parse(atob(token.split(".")[1]));
  return decodedToken.account.id;
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const accounts_id = getAccountIdFromToken();
    try {
      const response = await fetch(`${process.env.REACT_APP_API_HOST}/blog`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`,
        },
        body: JSON.stringify({ accounts_id:accounts_id, title:title
         }),
      });
      if (!response.ok) {
        const errorData = await response.json();
        throw new Error(`Server responded with ${response.status}: ${errorData.detail}`);
      }


      const createdBlog = await response.json();

      // console.log("Created Blog:", createdBlog);

      createdblogid= createdBlog.blogs_id;

      // console.log("Created Blog ID:", createdblogid);

      navigate(`/blog/${createdblogid}`);


    } catch (error) {
      console.error("An error occurred while creating a new blog", error);
    }
  }



return(
<>
      <div className="min-h-screen flex items-center justify-center bg-gray-100">
        <div className="bg-white p-8 rounded-lg shadow-md w-96">
        <h1 className="text-2xl mb-6 text-center font-bold">
            Create Your Blog!</h1>
            <form onSubmit={handleSubmit}>
              <div className="form-floating mb-3">
                 <label  className="block text-lg font-bold mb-2" htmlFor="Blog Title">Title</label>
                <input
                placeholder="Blog Title"
                type="text"
                name="Blog Title"
                id="Blog Title"
                className="shadow appearance-none border rounded w-full py-2 px-3 leading-tight focus:outline-none focus:shadow-outline"
                value={title}
                onChange={handleChangeTitle}
                required
                />

                </div>
              <button type="submit"  className="w-full bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">
                Submit
              </button>
            </form>
          </div>
        </div>

    </>
);







}
