import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import useToken from "@galvanize-inc/jwtdown-for-react";

const AddGardenForm = () => {
  const [formData, setFormData] = useState({
    gardens_name: "",
    description: "",
    zip_code: "",
  });

  const [validationError, setValidationError] = useState(null);
  const { token } = useToken();
  const navigate = useNavigate();

  const getAccountIdFromToken = () => {
    const decodedToken = JSON.parse(atob(token.split(".")[1]));
    return decodedToken.account.id;
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevState) => ({
      ...prevState,
      [name]: value,
    }));

    setValidationError(null);
  };

  const isValidZip = (zip) => {
    return /^\d{5}$/.test(zip);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    if (!isValidZip(formData.zip_code)) {
      setValidationError("Please enter a valid 5 digit zip code.");
      return;
    }

    const accounts_id = getAccountIdFromToken();
    try {
      const response = await fetch(`${process.env.REACT_APP_API_HOST}/api/gardens`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify({ ...formData, accounts_id }),
      });

      if (!response.ok) {
        const errorData = await response.json();
        throw new Error(
          `Server responded with ${response.status}: ${errorData.detail}`
        );
      }
      navigate("/gardens"); // Take the user back to the garden list
    } catch (error) {
      console.error("An error occurred while adding a new garden", error);
    }
  };

  return (
    <div className="min-h-screen flex items-center justify-center bg-gray-100">
      <div className="bg-white p-8 rounded-lg shadow-md w-96">
        <h1 className="text-2xl mb-6 text-center font-bold">
          Create a New Garden
        </h1>
        <form onSubmit={handleSubmit}>
          <div className="mb-4">
            <label
              className="block text-sm font-bold mb-2"
              htmlFor="gardens_name"
            >
              Garden Name:
            </label>
            <input
              type="text"
              id="gardens_name"
              name="gardens_name"
              className="shadow appearance-none border rounded w-full py-2 px-3 leading-tight focus:outline-none focus:shadow-outline"
              value={formData.gardens_name}
              onChange={handleChange}
              required
            />
          </div>
          <div className="mb-4">
            <label
              className="block text-sm font-bold mb-2"
              htmlFor="description"
            >
              Description:
            </label>
            <input
              type="text"
              id="description"
              name="description"
              className="shadow appearance-none border rounded w-full py-2 px-3 leading-tight focus:outline-none focus:shadow-outline"
              value={formData.description}
              onChange={handleChange}
              required
            />
          </div>
          <div className="mb-4">
            <label className="block text-sm font-bold mb-2" htmlFor="zip_code">
              Zip Code:
            </label>
            <input
              type="text"
              id="zip_code"
              name="zip_code"
              className="shadow appearance-none border rounded w-full py-2 px-3 leading-tight focus:outline-none focus:shadow-outline"
              value={formData.zip_code}
              onChange={handleChange}
              required
            />
            {validationError && (
              <p className="text-red-500 text-xs mt-1">{validationError}</p>
            )}
          </div>
          <div className="mt-4">
            <button
              type="submit"
              className="w-full bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
            >
              Add Garden
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default AddGardenForm;
