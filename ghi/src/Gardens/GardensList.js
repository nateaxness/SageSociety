import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import useToken from "@galvanize-inc/jwtdown-for-react";


const GardensList = () => {
  const [weatherDataMap, setWeatherDataMap] = useState({});
  const apiKey = "ae2343ff5eb4e3a75cd290b85e751367";
  const { token, fetchWithToken } = useToken();
  const [gardens, setGardens] = useState([]);
  // eslint-disable-next-line no-unused-vars
  const [account, setAccount] = useState(null);
  const fetchGardens = async () => {
    const { gardens } = await fetchWithToken(
      `${process.env.REACT_APP_API_HOST}/api/gardens`,
      "GET"
    );
    setGardens(gardens);
  };
  useEffect(() => {
    if (token) {
      fetchGardens();
      const account = JSON.parse(atob(token.split(".")[1])).account;
      setAccount(account);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [token]);

  useEffect(() => {
    gardens.forEach((gardens) => {
      loadWeatherData(gardens.zip_code, gardens.gardens_id);
    });
  }, [gardens]);

  async function loadWeatherData(zip_code, gardens_id) {
    try {
      const response = await fetch(
        `https://api.openweathermap.org/data/2.5/weather?zip=${zip_code},us&units=imperial&appid=${apiKey}`
      );
      const data = await response.json();
      setWeatherDataMap((prevState) => ({ ...prevState, [gardens_id]: data }));
    } catch (error) {
      console.error("Error occurred while fetching weather data", error);
    }
  }

  async function deleteGarden(gardens_id) {
    const userConfirmed = window.confirm("Are you sure you want to delete this garden?");
    if (userConfirmed) {
      try {
        const isSuccess = await fetchWithToken(
          `${process.env.REACT_APP_API_HOST}/api/gardens/${gardens_id}`,
          "DELETE"
        );
        if (isSuccess) {
          setGardens(
            gardens.filter((garden) => garden.gardens_id !== gardens_id)
          );
        } else {
          console.error(`Failed to delete garden with ID ${gardens_id}`);
        }
      } catch (error) {
        console.error(`Error deleting garden with ID ${gardens_id}:`, error);
      }
    }
  }
  return (
    <div className="min-h-screen flex items-center justify-center bg-gray-100">
      <div className="bg-white p-8 rounded-lg shadow-md w-full max-w-2xl">
        <h1 className="text-3xl mb-6 text-center font-bold">My Gardens</h1>

        <ul>
          {gardens.map((gardens) => (
            <li
              key={gardens.gardens_id}
              className="mb-4 bg-gray-100 p-4 rounded shadow"
            >
              <h2 className="text-xl font-bold mb-2">
                <Link to={`/gardens/${gardens.gardens_id}/plants`}>
                  {gardens.gardens_name}
                </Link>
              </h2>
              <p className="mb-2">Garden Description: {gardens.description}</p>
              {weatherDataMap[gardens.gardens_id] && (
                <div className="weather-info mb-2">
                  <h3 className="text-md font-semibold">
                    Here are the weather conditions at your {gardens.gardens_name} garden:
                  </h3>
                  <p>
                    Condition:{" "}
                    {weatherDataMap[gardens.gardens_id].weather[0].description}
                  </p>
                  <p>
                    Temp: {weatherDataMap[gardens.gardens_id].main.temp}
                    °F
                  </p>
                  <p>
                    Humidity: {weatherDataMap[gardens.gardens_id].main.humidity}
                    %
                  </p>
                </div>
              )}
              <div className="text-right">
                <Link to={`/gardens/${gardens.gardens_id}/edit`} className="inline-block">
                  <button className="bg-yellow-500 hover:bg-yellow-600 text-white font-bold py-0 px-1 rounded focus:outline-none focus:shadow-outline">
                    Edit
                  </button>
                </Link>
                <button
                  onClick={() => deleteGarden(gardens.gardens_id)}
                  className="bg-red-500 hover:bg-red-600 text-white font-bold py-0 px-1 rounded focus:outline-none focus:shadow-outline ml-2">
                  Delete
                </button>
                </div>
                <div className="text-left mt-2">
                  <span>Click </span>
                  <a
                    href={`https://www.google.com/maps/search/soil+testing+${gardens.zip_code}`}
                    target="_blank"
                    rel="noopener noreferrer"
                    style={{ color: 'blue', textDecoration: 'underline' }}
                  >
                    here
                  </a>
                  <span> to find soil pH testing near this garden</span>
                </div>
            </li>
          ))}
        </ul>
        <Link to="/gardens/add" className="block mb-6 text-center">
          <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-1 px-3 rounded focus:outline-none focus:shadow-outline">
            Add Garden
          </button>
        </Link>
      </div>
    </div>
  );
};

export default GardensList;
