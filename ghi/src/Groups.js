import { useState } from "react";

export default function Groups() {
  const [groupName, setGroupName] = useState("");
  const [description, setDescription] = useState("");
  const [groupDetails, setGroupDetails] = useState(null);
  const [error, setError] = useState(null);
  const [allGroups, setAllGroups] = useState([]);
  const [deleteStatus, setDeleteStatus] = useState(null);
  const [searchGroupName, setSearchGroupName] = useState("");

  const createGroup = async () => {
    const response = await fetch(`${process.env.REACT_APP_API_HOST}/groups`, {
      method: "POST",
      credentials: "include",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        groups_name: groupName,
        description: description,
      }),
    });

    const data = await response.json();

    if (data.groups_id) {
      console.log("Group created:", data);
      setGroupName("");
      setDescription("");
    } else {
      setError(data.message);
    }
  };

  const fetchGroupDetails = async () => {
    const response = await fetch(
      `${process.env.REACT_APP_API_HOST}/groups/name/${searchGroupName}`,
      {
        credentials: "include",
      }
    );

    const data = await response.json();

    if (data.groups_id) {
      setGroupDetails(data);
    } else {
      setError(data.message);
    }
  };
  const listAllGroups = async () => {
    const response = await fetch(`${process.env.REACT_APP_API_HOST}/groups`, {
      credentials: "include",
    });

    const data = await response.json();

    if (Array.isArray(data)) {
      setAllGroups(data);
    } else {
      setError(data.message);
    }
  };

  const deleteGroup = async (id) => {
    const response = await fetch(
      `${process.env.REACT_APP_API_HOST}/groups/${id}`,
      {
        method: "DELETE",
        credentials: "include",
      }
    );

    const data = await response.json();

    if (data.message) {
      if (data.message.includes("successfully")) {
        // If successfully deleted, also update the allGroups state to reflect the change
        setAllGroups((prevGroups) =>
          prevGroups.filter((group) => group.groups_id !== id)
        );
      }
      setDeleteStatus(data.message);
    }
  };
  return (
    <div className="min-h-screen flex items-center justify-center bg-gray-100">
      <div className="bg-white p-8 rounded-lg shadow-md w-96">
        <h2 className="text-2xl mb-6 text-center font-bold">
          Groups Management
        </h2>
        {/* Create Group Section */}
        <form
          onSubmit={(e) => {
            e.preventDefault();
            createGroup();
          }}
        >
          <div className="mb-4">
            <label className="block text-sm font-bold mb-2" htmlFor="groupName">
              Group Name:
            </label>
            <input
              id="groupName"
              type="text"
              className="shadow appearance-none border rounded w-full py-2 px-3 leading-tight focus:outline-none focus:shadow-outline"
              value={groupName}
              onChange={(e) => setGroupName(e.target.value)}
              placeholder="Group Name"
            />
          </div>

          <div className="mb-4">
            <label
              className="block text-sm font-bold mb-2"
              htmlFor="description"
            >
              Description (Optional):
            </label>
            <textarea
              id="description"
              className="shadow appearance-none border rounded w-full py-2 px-3 leading-tight focus:outline-none focus:shadow-outline"
              value={description}
              onChange={(e) => setDescription(e.target.value)}
              placeholder="Description"
            />
          </div>

          <div className="mt-4">
            <button
              type="submit"
              className="w-full bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
            >
              Create Group
            </button>
          </div>
        </form>

        {/* Fetch Group Details Section */}
        <form
          onSubmit={(e) => {
            e.preventDefault();
            fetchGroupDetails();
          }}
          className="mt-6"
        >
          <div className="mb-4">
            <label
              className="block text-sm font-bold mb-2"
              htmlFor="searchGroupName"
            >
              Get Group Details by Name:
            </label>
            <input
              id="searchGroupName"
              type="text"
              className="shadow appearance-none border rounded w-full py-2 px-3 leading-tight focus:outline-none focus:shadow-outline"
              value={searchGroupName}
              onChange={(e) => setSearchGroupName(e.target.value)}
              placeholder="Group Name"
            />
          </div>

          <div className="mt-4">
            <button
              type="submit"
              className="w-full bg-gray-500 hover:bg-gray-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
            >
              Fetch Details
            </button>
          </div>
        </form>

        {/* Display Group Details */}
        {groupDetails && (
          <div className="mt-6">
            <h6 className="text-lg font-semibold mb-2">Group Details:</h6>
            <p>ID: {groupDetails.groups_id}</p>
            <p>Name: {groupDetails.groups_name}</p>
            <p>Description: {groupDetails.description || "N/A"}</p>
          </div>
        )}
        {error && <p className="mt-3 text-red-600">Error: {error}</p>}
      </div>
      {/* List All Groups Button */}
      <div className="mt-4">
        <button
          onClick={listAllGroups}
          className="w-full bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
        >
          List All Groups
        </button>
      </div>

      {/* Display All Groups */}
      {allGroups.length > 0 && (
        <div className="mt-6">
          <h6 className="text-lg font-semibold mb-2">All Groups:</h6>
          <ul>
            {allGroups.map((group) => (
              <li key={group.groups_id} className="mb-2">
                <span>
                  {group.groups_name} - {group.description || "N/A"}
                </span>
                <button
                  onClick={() => deleteGroup(group.groups_id)}
                  className="ml-4 text-red-600 hover:text-red-800"
                >
                  Delete
                </button>
              </li>
            ))}
          </ul>
        </div>
      )}
      {deleteStatus && <p className="mt-3 text-blue-600">{deleteStatus}</p>}
    </div>
  );
}
