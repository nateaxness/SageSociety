import React, { useState, useEffect, useRef, useCallback } from "react";
import { useParams } from "react-router-dom";
import { useLocation } from "react-router-dom";

const PlantsPage = ({ zipCode }) => {
  /* eslint-disable-next-line no-unused-vars */
  const OPENAI_API_KEY = "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";
  const [reloadToken, setReloadToken] = useState(null);
  const [careInstructions, setCareInstructions] = useState({});
  /* eslint-disable-next-line no-unused-vars */
  const [isFormValid, setIsFormValid] = useState(false);
  /* eslint-disable-next-line no-unused-vars  */
  const location = useLocation();
  const { gardens_id } = useParams();
  const [plants, setPlants] = useState([]);
  const [error, setError] = useState(null);
  const [plantDetailsMap, setPlantDetailsMap] = useState({});
  const [showForm, setShowForm] = useState(false);
  const [newPlant, setNewPlant] = useState({
    plants_name: "",
    description: "",
    planted_date: "",
  });
  const [editingPlant, setEditingPlant] = useState(null);
  const [imagePreview, setImagePreview] = useState(null);
  const fileInput = useRef(null);
  console.log("API Host:", process.env.REACT_APP_API_HOST);

  async function fetchCareInstructions(plants_name, zip_code) {
    try {
      const response = await fetch(
        `${process.env.REACT_APP_API_HOST}/getCareInstructions/${plants_name}/${zip_code}`,
        {
          method: "POST",
          headers: {
            Authorization: `Bearer ${process.env.OPENAI_API_KEY}`,
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            max_tokens: 150,
          }),
        }
      );

      if (!response.ok) {
        throw new Error(`Server responded with a ${response.status} status.`);
      }

      const data = await response.json();

      // Safely accessing the message content
      const content = data?.choices?.[0]?.message?.content;

      if (content) {
        setCareInstructions((prev) => ({
          ...prev,
          [plants_name]: content.trim(),
        }));
      } else {
        console.error("Unexpected data format from the server:", data);
      }
    } catch (error) {
      console.error("Error fetching care instructions:", error.message);
    }
  }

  async function fetchPlantDetails(plants_name, plants_id) {
    try {
      const response = await fetch(
        `${process.env.REACT_APP_API_HOST}/fetch_plant/${plants_name}`
      );
      const data = await response.json();

      if (data && data.data && data.data.length > 0) {
        const plantDetails = data.data[0];
        setPlantDetailsMap((prev) => ({ ...prev, [plants_id]: plantDetails }));
      } else {
        throw new Error("No details found for this plant.");
      }
    } catch (err) {
      console.error(
        "An error occurred fetching the plant details:",
        err.message
      );
    }
  }
  function handleAdd() {
    setShowForm(!showForm);
  }
  function handleImageChange(e) {
    const file = e.target.files[0];
    if (file) {
      const reader = new FileReader();

      reader.onloadend = () => {
        setImagePreview(reader.result);
      };

      reader.readAsDataURL(file);

      setNewPlant((prev) => ({
        ...prev,
        image: file,
      }));

      setIsFormValid(true);
    } else {
      setIsFormValid(false);
    }
  }

  async function handleSubmit() {
    if (
      !newPlant.plants_name ||
      !newPlant.description ||
      !newPlant.planted_date ||
      !imagePreview
    ) {
      setError("All fields must be filled out.");
      return;
    }

    try {
      const plantPayload = new FormData();

      const baseUrl = `${process.env.REACT_APP_API_HOST}/gardens/${gardens_id}/plants/`;

      const queryParams = new URLSearchParams({
        plants_name: newPlant.plants_name,
        description: newPlant.description,
        planted_date: newPlant.planted_date,
      });

      console.log(newPlant.image);
      plantPayload.append("image_file", newPlant.image);

      let response;
      if (editingPlant) {
        response = await fetch(
          `${baseUrl}${editingPlant.plants_id}/?${queryParams.toString()}`,
          {
            method: "PUT",
            credentials: "include",
            body: plantPayload,
          }
        );
      } else {
        response = await fetch(`${baseUrl}?${queryParams.toString()}`, {
          method: "POST",
          credentials: "include",
          body: plantPayload,
        });
      }

      const responseData = await response.json();

      if (!response.ok) {
        throw new Error(
          editingPlant
            ? `Failed to update plant: ${responseData.message}`
            : `Failed to add plant: ${responseData.message}`
        );
      }
      setNewPlant({
        plants_name: "",
        description: "",
        planted_date: "",
      });
      setImagePreview(null);
      setEditingPlant(null);
      setShowForm(false);
      fetchPlants();
      setPlantDetailsMap({});
      setError(null);
      setIsFormValid(true);
    } catch (error) {
      console.error(
        editingPlant ? "Error updating plant" : "Error adding plant",
        error
      );
    }
  }

  async function handleDelete(plantId) {
    try {
      const response = await fetch(
        `${process.env.REACT_APP_API_HOST}/gardens/${gardens_id}/plants/${plantId}`,
        {
          method: "DELETE",
          credentials: "include",
        }
      );

      if (!response.ok) {
        throw new Error("Failed to delete plant");
      }

      fetchPlants();
    } catch (error) {
      console.error("Error deleting plant", error);
    }
  }
  console.log("API Host:", process.env.REACT_APP_API_HOST);
  function handleEdit(plant) {
    setEditingPlant(plant);
    setNewPlant(plant);
    setShowForm(true);
  }
  console.log("API Host:", process.env.REACT_APP_API_HOST);
  const fetchPlants = useCallback(async () => {
    try {
      const response = await fetch(
        `${process.env.REACT_APP_API_HOST}/gardens/${gardens_id}/plants/`,
        {
          credentials: "include",
        }
      );
      const data = await response.json();

      if (response.ok) {
        setPlants(data);
        setReloadToken(new Date().getTime());
      } else {
        throw new Error(data.message || "An error occurred");
      }
    } catch (err) {
      setError(err.message);
      console.error("An error occurred fetching the plants data:", err.message);
    }
    /* eslint-disable-next-line react-hooks/exhaustive-deps */
  }, [gardens_id]);

  useEffect(() => {
    fetchPlants();
    /* eslint-disable-next-line react-hooks/exhaustive-deps */
  }, []);

  const filteredPlants = plants.filter(
    (plant) => plant.gardens_id === parseInt(gardens_id)
  );

  return (
    <div className="min-h-screen flex items-center justify-center bg-gray-100">
      <div className="bg-white p-8 rounded-lg shadow-md w-full max-w-2xl">
        <h1 className="text-3xl mb-6 text-center font-bold">
          Plants in Garden {gardens_id}
        </h1>

        {error && <p className="text-red-500">{error}</p>}

        <button
          className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline mb-6"
          onClick={handleAdd}
        >
          Add Plant
        </button>

        {showForm && (
          <form
            onSubmit={(e) => {
              e.preventDefault();
              handleSubmit();
            }}
            className="mb-6"
          >
            <div className="mb-4">
              <input
                value={newPlant.plants_name}
                onChange={(e) =>
                  setNewPlant((prev) => ({
                    ...prev,
                    plants_name: e.target.value,
                  }))
                }
                placeholder="Plant Name"
                className="w-full p-2 border rounded"
              />
              <textarea
                value={newPlant.description}
                onChange={(e) =>
                  setNewPlant((prev) => ({
                    ...prev,
                    description: e.target.value,
                  }))
                }
                placeholder="Description"
                className="w-full p-2 border rounded mt-2"
              />
              <input
                type="date"
                value={newPlant.planted_date}
                onChange={(e) =>
                  setNewPlant((prev) => ({
                    ...prev,
                    planted_date: e.target.value,
                  }))
                }
                className="w-full p-2 border rounded mt-2"
              />
              <button
                type="submit"
                className="bg-green-500 hover:bg-green-600 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline mt-4"
              >
                {editingPlant ? "Update" : "Add"}
              </button>
            </div>

            <div className="mb-4">
              <input
                type="file"
                onChange={handleImageChange}
                style={{ display: "none" }}
                ref={fileInput}
              />
              <button
                type="button"
                className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                onClick={() => fileInput.current.click()}
              >
                Upload
              </button>

              {imagePreview && (
                <img
                  src={imagePreview}
                  alt="Preview"
                  className="mt-4 w-72 h-72 object-cover mx-auto"
                />
              )}
            </div>
          </form>
        )}

        <ul>
          {filteredPlants.map((plant) => (
            <li
              key={plant.plants_id}
              className="mb-4 bg-gray-100 p-4 rounded shadow"
            >
              <h2 className="text-xl font-bold mb-2">{plant.plants_name}</h2>
              <p className="mb-2">{plant.description}</p>
              <p className="mb-2">{plant.planted_date}</p>
              <img
                src={`${process.env.REACT_APP_API_HOST}/gardens/${gardens_id}/plants/${plant.plants_id}/image/?reload=${reloadToken}`}
                alt={plant.plants_name}
                className="w-72 h-72 object-cover mx-auto mb-2"
              />
              <button
                onClick={() => handleEdit(plant)}
                className="bg-yellow-500 hover:bg-yellow-600 text-white font-bold py-1 px-3 rounded focus:outline-none focus:shadow-outline mr-2"
              >
                Edit
              </button>
              <button
                onClick={() =>
                  fetchCareInstructions(plant.plants_name, zipCode)
                }
                className="bg-green-500 hover:bg-green-600 text-white font-bold py-1 px-3 rounded focus:outline-none focus:shadow-outline mr-2"
              >
                Care Instructions
              </button>
              <button
                onClick={() => handleDelete(plant.plants_id)}
                className="bg-red-500 hover:bg-red-600 text-white font-bold py-1 px-3 rounded focus:outline-none focus:shadow-outline mr-2"
              >
                Delete
              </button>
              <button
                onClick={() =>
                  fetchPlantDetails(plant.plants_name, plant.plants_id)
                }
                className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-1 px-3 rounded focus:outline-none focus:shadow-outline"
              >
                See Details
              </button>

              {plantDetailsMap[plant.plants_id] && (
                <div className="mt-4">
                  <h3 className="text-lg font-semibold mb-2">
                    {plantDetailsMap[plant.plants_id].common_name}
                  </h3>
                  <img
                    src={plantDetailsMap[plant.plants_id].image_url}
                    alt={plantDetailsMap[plant.plants_id].common_name}
                  />
                  <p className="mb-2">
                    {plantDetailsMap[plant.plants_id].family}
                  </p>
                </div>
              )}

              {careInstructions[plant.plants_name] && (
                <div className="mt-4">
                  <h4 className="text-lg font-semibold mb-2">
                    Care Instructions:
                  </h4>
                  <p>{careInstructions[plant.plants_name]}</p>
                </div>
              )}
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
};
export default PlantsPage;
