function MainPage() {
  return (
    <>
      <div className="p-8 flex flex-col items-center justify-center min-h-screen bg-black bg-opacity-80">
        <h1 className="text-6xl lg:text-7xl capitalize mb-12 text-white font-semibold">
          We up
        </h1>
        <div className="col-lg-6 mx-auto">
          <p className="text-xl text-gray-300 mb-4">Hey we did it</p>
        </div>
      </div>
    </>
  );
}
export default MainPage;
