# Nate Axness | Sage Society Development Journal

### Week 17 Entry:

* 09/08/2023
* Finalized GardensList.js, created AddGardenForm.js and EditGardenForm.js.
* Collaborated with Nick de la Flor and getting GardensList to work with PlantsPage.js.
* Dove into the codebase to understand how other's team features compared to mine and how thier's worked. Got more confortable using the network tab in inspect tools to debug. Got more familiar with how the token actually works and what data is embedded in it. I realized one of my endpoints was redundant because I was extracting account_id info from the token.

### Week 16 Entry:

* 08/30/2023
* Worked on GardensList.js front-end React component.
* Worked with Nick de la Flor
* Installed dependencies that I ended up not needing, and through a merge request I possibly corrupted my package-lock.json file. Learned how to delete node modules and package-lock.json files and rebuild them.
---
### Week 15 Entry:

* 08/25/2023
* Finalized queries/gardens.py and routers/gardens.py.
* Solo
* Previously I wrote queries and routers for accounts mostly via template, so writing gardens allowed me much more understanding of how the FastAPI architechture works vs my only other exposure - Django.
---
### Week 14 Entry:

* 08/18/2023
* Wrote queries/accounts.py and routers/accounts.py. Brainstormed with dev team on Sage Society wireframe.
* Nicholas de la Flor, Joshua Scott, Alec Weinstein
* Realized the importance of a proper planning phase.
---
