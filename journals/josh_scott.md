# Joshua Scott | Sage Society Development Journal

### Week 17 Entry:

- 09/11/2023
- deployed front end via gitlab-ci.yml file allowing CI/CD pages job to build and pass
- created react app api host var in CI/CD variables in gitlab to properly set api host for front end communication preventing cors errors
- added technical readme.md
- trouble shooting

---

### Week 16 Entry:

---

- 09/05/2023
- Built unit tests for test_groups.py
- Deployed back end api and db service using glv-cloud-cli
- built front end components for groups.js
- tailwindCSS design for nav bar and individual components. Signup form, loginform, logout button, groups component.

### Week 15 Entry:

- 08/28/2023
- Built front accounts components signup/login/logout with correct front end auth implementation via learn and notion documenation

---

### Week 14 Entry:

- 08/21/2023
- Built back end auth for accounts. signup/login/logout/modify/delete
- Finished groups, groups_members, groups_comments routers/queries as well as adding authentication on individual routes (depends(authenticator.get_current_account_data))

---

### Week 13 Entry:

- 08/14/2023
- established group familiarity/competencey with version control using Git
- drew git workflow diagrams in excalidraw
- trello wireframing

---

### Week 12 Entry:

- 08/07/2023
- proposed app high level overview, features, functionality
- assigned individual work load

---
