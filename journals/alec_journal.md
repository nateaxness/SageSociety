# Alec Weinstein | Sage Society Development Journal


### 9/3-9/11 Entry:

* Completed frontend components for Creating a Blog, Blog Detail, Create BlogPost and Get All Blogposts
* Considered architecture for props passing and refactoring files so that they exist in one, will work on this next week.
* Discovered that I never had node.js installed on my computer when I attempted to install tailwind through an npm command and fixed that.
* Identified achievable stretch goals to be accomplished the coming week.

---

### 8/27-9/3 Entry:

* Learned how to alter default formatters in settings.json( I was using prettier instead of black)
* Completed backend, and remembered the different methods of passing in values through both a request body and parameters



### 8/20-8/27 Entry:


---





### 8/13-8/20 Entry:

* Brainstormed app ideas, came to consensus, then tempered expectations suitable for a MVP

---
