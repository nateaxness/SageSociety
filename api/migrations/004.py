steps = [
    # add image data column
    [
        """
        ALTER TABLE plants ADD COLUMN image_data BYTEA;
        """,
        """
        ALTER TABLE plants DROP COLUMN image_data;
        """,
    ],
]
