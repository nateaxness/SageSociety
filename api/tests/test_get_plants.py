from main import app, FastAPI
from fastapi.testclient import TestClient
from queries.plants import PlantRepository, PlantOut
from datetime import date
from authenticator import authenticator
from queries.accounts import AccountOut

client = TestClient(app)


class MockedRepository:
    # Get All Plants
    def get_all(self, gardens_id: int):
        return [
            PlantOut(
                plants_id=1,
                plants_name="Rose",
                planted_date=date.today(),
                description="A red rose.",
                gardens_id=gardens_id,
            )
        ]

    # Get One Plant
    def get_one(self, plants_id: int, gardens_id: int): 
        if plants_id == 1 and gardens_id == 1: 
            return PlantOut(
                plants_id=1,
                plants_name="Rose",
                planted_date=date.today(),
                description="A red rose.",
                gardens_id=1,
            )
        return None

def mock_get_current_account_data():
    return AccountOut(
        id=1,
        first_name="John",
        last_name="Doe",
        username="testuser",
        email="test@example.com",
        phone_number="1214567890",
    )


def test_get_all_plants_in_garden():
    # Arrange
    FastAPI().dependency_overrides = {}
    app.dependency_overrides[PlantRepository] = MockedRepository
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = mock_get_current_account_data  # Add this line

    # Act
    response = client.get("/gardens/1/plants/")
    app.dependency_overrides = {}

    # Assert
    assert response.status_code == 200
    assert response.json() == [
        {
            "plants_id": 1,
            "plants_name": "Rose",
            "planted_date": str(date.today()),
            "description": "A red rose.",
            "gardens_id": 1,
        }
    ]


def test_get_single_plant_in_garden():
    # Arrange
    FastAPI().dependency_overrides = {}
    app.dependency_overrides[PlantRepository] = MockedRepository
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = mock_get_current_account_data  # Add this line

    # Act
    response = client.get("/gardens/1/plants/1/")

    # Clean up
    app.dependency_overrides = {}
    FastAPI().dependency_overrides = {}

    # Assert
    assert response.status_code == 200
    assert response.json() == {
        "plants_id": 1,
        "plants_name": "Rose",
        "planted_date": str(date.today()),
        "description": "A red rose.",
        "gardens_id": 1,
    }
