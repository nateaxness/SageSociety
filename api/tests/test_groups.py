from main import app
from fastapi import FastAPI
from fastapi.testclient import TestClient
from queries.groups import GroupRepository, GroupOut, GroupIn, Error
from queries.accounts import AccountOut
from authenticator import authenticator
from typing import Union
from unittest.mock import patch


client = TestClient(app)


class MockedGroupRepository:
    def create_group(self, groups_data: GroupIn) -> Union[GroupOut, Error]:
        print("Creating group:", groups_data.dict())
        return GroupOut(
            groups_id=1,
            groups_name=groups_data.groups_name,
            description=groups_data.description,
        )


def mock_get_current_account_data():
    return AccountOut(
        id=1,
        first_name="John",
        last_name="Doe",
        username="testuser",
        email="test@example.com",
        phone_number="1214567890",
    )


@patch.object(
    GroupRepository, "create_group", new=MockedGroupRepository().create_group
)
def test_create_group():
    # Arrange
    FastAPI().dependency_overrides = {}

    sample_group = {
        "groups_name": "Sample Group",
        "description": "This is a sample group.",
    }

    expected_response = {
        "groups_id": 1,
        "groups_name": "Sample Group",
        "description": "This is a sample group.",
    }

    app.dependency_overrides[GroupRepository] = MockedGroupRepository
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = mock_get_current_account_data

    # Act
    response = client.post("/groups", json=sample_group)

    # Clean up
    app.dependency_overrides = {}
    FastAPI().dependency_overrides = {}

    # Assert
    assert response.status_code == 201
    assert response.json() == expected_response
