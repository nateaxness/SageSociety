from main import app
from fastapi.testclient import TestClient
from queries.gardens import GardenQueries, GardenOut
from authenticator import authenticator


def get_mock_current_account_data():
    return {"id": 1, "username": "mock_username"}


class MockGardenQueries:
    def get_all_gardens(self, accounts_id):
        return [
            GardenOut(
                gardens_id=1,
                gardens_name="Garden 1",
                description="Desc 1",
                accounts_id=1,
                zip_code=30265,
            ),
            GardenOut(
                gardens_id=2,
                gardens_name="Garden 2",
                description="Desc 2",
                accounts_id=1,
                zip_code=90210,
            ),
        ]


client = TestClient(app)


def test_get_all_gardens():
    # Arrange
    app.dependency_overrides[GardenQueries] = MockGardenQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = get_mock_current_account_data

    # Act
    response = client.get("/api/gardens")
    app.dependency_overrides.clear()

    # Assert
    assert response.status_code == 200
    assert response.json() == {
        "gardens": [
            {
                "gardens_id": 1,
                "gardens_name": "Garden 1",
                "description": "Desc 1",
                "accounts_id": 1,
                "zip_code": 30265,
            },
            {
                "gardens_id": 2,
                "gardens_name": "Garden 2",
                "description": "Desc 2",
                "accounts_id": 1,
                "zip_code": 90210,
            },
        ]
    }
