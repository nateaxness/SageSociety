from main import app
from fastapi import FastAPI
from fastapi.testclient import TestClient
from queries.blogs import BlogRepository, BlogOut
from authenticator import authenticator
from unittest.mock import patch

client = TestClient(app)


class MockedBlogRepository:
    def get_one_blog(self, blogs_id: int) -> BlogOut:
        print("Fetching blog:", blogs_id)
        if blogs_id == 1:
            return BlogOut(blogs_id=1, accounts_id=1, title="My Garden Blog")
        return None


def mock_get_current_account_data():
    return {"id": 1, "username": "mock_username"}


@patch.object(
    BlogRepository, "get_one_blog", new=MockedBlogRepository().get_one_blog
)
def test_get_one_blog():
    # Arrange
    FastAPI().dependency_overrides = {}

    expected_response = {
        "blogs_id": 1,
        "accounts_id": 1,
        "title": "My Garden Blog",
    }

    app.dependency_overrides[BlogRepository] = MockedBlogRepository
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = mock_get_current_account_data

    # Act
    response = client.get("/blog/1")

    # Cleanup
    app.dependency_overrides = {}
    FastAPI().dependency_overrides = {}

    # Assert
    assert response.status_code == 200
    assert response.json() == expected_response
