from pydantic import BaseModel
from typing import Union, List
from queries.pool import pool


class Error(BaseModel):
    message: str


class GroupMemberIn(BaseModel):
    groups_id: int
    accounts_id: int


class GroupMemberOut(BaseModel):
    groups_id: int
    accounts_id: int


class GroupMemberRepository:
    def add_member(
        self, member_data: GroupMemberIn
    ) -> Union[GroupMemberOut, Error]:
        query = """
            INSERT INTO groups_members (groups_id, accounts_id)
            VALUES (%s, %s)
            RETURNING groups_id, accounts_id;
        """
        try:
            with pool.connection() as conn:
                with conn.cursor() as cursor:
                    cursor.execute(
                        query, (member_data.groups_id, member_data.accounts_id)
                    )
                    columns = ["groups_id", "accounts_id"]
                    row = cursor.fetchone()
                    row_dict = dict(zip(columns, row))
                    return GroupMemberOut(**row_dict)
        except Exception as e:
            print(e)
            return {"message": "Failed to add member to group."}

    def get_members(
        self, groups_id: int
    ) -> Union[List[GroupMemberOut], Error]:
        query = """
            SELECT groups_id, accounts_id
            FROM groups_members
            WHERE groups_id = %s;
        """
        try:
            with pool.connection() as conn:
                with conn.cursor() as cursor:
                    cursor.execute(query, (groups_id,))
                    results = cursor.fetchall()
                    columns = ["groups_id", "accounts_id"]
                    return [
                        GroupMemberOut(**dict(zip(columns, row)))
                        for row in results
                    ]
        except Exception as e:
            print(e)
            return {"message": "Failed to retrieve group members."}
