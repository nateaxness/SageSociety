from pydantic import BaseModel
from typing import List, Optional
from queries.pool import pool


class DuplicateAccountError(ValueError):
    message: str


class AccountIn(BaseModel):
    first_name: str
    last_name: str
    username: str
    password: str
    email: str
    phone_number: str


class AccountUpdate(BaseModel):
    first_name: Optional[str]
    last_name: Optional[str]
    username: Optional[str]
    password: Optional[str]
    email: Optional[str]
    phone_number: Optional[str]


class AccountOut(BaseModel):
    id: int
    first_name: str
    last_name: str
    username: str
    email: str
    phone_number: str


class AccountOutWithPassword(AccountOut):
    hashed_password: str


class AccountRepository:
    def create_account(
        self, account: AccountIn, hashed_password: str
    ) -> AccountOutWithPassword:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO accounts
                            (first_name, last_name, username, hashed_password, email, phone_number)
                        VALUES (%s, %s, %s, %s, %s, %s)
                        RETURNING id;
                        """,
                        [
                            account.first_name,
                            account.last_name,
                            account.username,
                            hashed_password,
                            account.email,
                            account.phone_number,
                        ],
                    )
                    id = result.fetchone()[0]
                    data = account.dict()
                    return AccountOutWithPassword(
                        id=id, **data, hashed_password=hashed_password
                    )
        except Exception:
            return {"Error": "Could not create Account"}

    def get_account(self, username_email: str) -> AccountOutWithPassword:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, first_name, last_name, username, hashed_password, email, phone_number
                        FROM accounts
                        WHERE username = %s OR email = %s
                        """,
                        [username_email, username_email],
                    )
                    account = result.fetchone()
                    if account is None:
                        return None
                    return AccountOutWithPassword(
                        id=account[0],
                        first_name=account[1],
                        last_name=account[2],
                        username=account[3],
                        hashed_password=account[4],
                        email=account[5],
                        phone_number=account[6],
                    )
        except Exception:
            return {"Error": "Could not get account by username or email"}

    def get_account_by_id(self, account_id: int) -> AccountOutWithPassword:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, first_name, last_name, username, hashed_password, email, phone_number
                        FROM accounts
                        WHERE id = %s
                        """,
                        [account_id],
                    )
                    account = result.fetchone()
                    if account is None:
                        return None
                    return AccountOutWithPassword(
                        id=account[0],
                        first_name=account[1],
                        last_name=account[2],
                        username=account[3],
                        hashed_password=account[4],
                        email=account[5],
                        phone_number=account[6],
                    )
        except Exception:
            return {"Error": "Could not get account by id"}

    def get_all(self) -> List[AccountOutWithPassword]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT id, first_name, last_name, username, hashed_password, email, phone_number
                        FROM accounts
                        ORDER BY id;
                        """
                    )
                    accounts = []
                    for account in db:
                        account = AccountOutWithPassword(
                            id=account[0],
                            first_name=account[1],
                            last_name=account[2],
                            username=account[3],
                            hashed_password=account[4],
                            email=account[5],
                            phone_number=account[6],
                        )
                        accounts.append(account)
                    return accounts
        except Exception:
            return {"Error": "Could not get all Accounts"}

    def update_account(
        self, account_id: int, account: AccountUpdate, hashed_password: str
    ) -> AccountOutWithPassword:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE accounts
                        SET first_name = %s, last_name = %s, username = %s, hashed_password = %s, email = %s, phone_number = %s
                        WHERE id = %s;
                        """,
                        [
                            account.first_name,
                            account.last_name,
                            account.username,
                            hashed_password,
                            account.email,
                            account.phone_number,
                            account_id,
                        ],
                    )
                    data = account.dict()
                    return AccountOutWithPassword(
                        id=account_id, **data, hashed_password=hashed_password
                    )
        except Exception:
            return {"Error": "Could not update Account"}

    def delete_account(self, account_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM accounts
                        WHERE id = %s
                        """,
                        [account_id],
                    )
        except Exception as e:
            print(e)
            return {"Error": "Could not delete Account"}
