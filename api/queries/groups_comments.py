from pydantic import BaseModel
from typing import Union, List
from queries.pool import pool
from datetime import datetime


class Error(BaseModel):
    message: str


class GroupCommentIn(BaseModel):
    content: str
    authors_id: int
    groups_id: int


class GroupCommentOut(BaseModel):
    comments_id: int
    content: str
    authors_id: int
    groups_id: int
    created_at: datetime


class GroupCommentRepository:
    def create_comment(
        self, comments_data: GroupCommentIn
    ) -> Union[GroupCommentOut, Error]:
        query = """
            INSERT INTO groups_comments (content, authors_id, groups_id)
            VALUES (%s, %s, %s)
            RETURNING comments_id, content, authors_id, groups_id, created_at;
        """
        try:
            with pool.connection() as conn:
                with conn.cursor() as cursor:
                    cursor.execute(
                        query,
                        (
                            comments_data.content,
                            comments_data.authors_id,
                            comments_data.groups_id,
                        ),
                    )
                    columns = [
                        "comments_id",
                        "content",
                        "authors_id",
                        "groups_id",
                        "created_at",
                    ]
                    row = cursor.fetchone()
                    print(row)

                if not row:
                    raise ValueError("No rows were returned from the query.")
                row_dict = dict(zip(columns, row))
                return GroupCommentOut(**row_dict)

        except Exception as e:
            print(f"Error: {e}")
            return {"message": "Failed to create comment."}

    def get_comment(self, comments_id: int) -> Union[GroupCommentOut, Error]:
        query = """
            SELECT comments_id, content, authors_id, groups_id, created_at
            FROM groups_comments
            WHERE comments_id = %s;
        """
        try:
            with pool.connection() as conn:
                cursor = conn.cursor()
                columns = [
                    "comments_id",
                    "content",
                    "authors_id",
                    "groups_id",
                    "created_at",
                ]
                cursor.execute(query, (comments_id,))
                row = cursor.fetchone()
                if not row:
                    return {"message": "Comment not found."}
                row_dict = dict(zip(columns, row))
                return GroupCommentOut(**row_dict)
        except Exception as e:
            print(e)
            return {"message": "Failed to retrieve comment."}

    def get_comments_for_group(
        self, groups_id: int
    ) -> Union[List[GroupCommentOut], Error]:
        query = """
            SELECT comments_id, content, authors_id, groups_id, created_at
            FROM groups_comments
            WHERE groups_id = %s;
        """
        try:
            with pool.connection() as conn:
                cursor = conn.cursor()
                columns = [
                    "comments_id",
                    "content",
                    "authors_id",
                    "groups_id",
                    "created_at",
                ]
                cursor.execute(query, (groups_id,))
                results = cursor.fetchall()
                return [
                    GroupCommentOut(**dict(zip(columns, result)))
                    for result in results
                ]
        except Exception as e:
            print(e)
            return {"message": "Failed to retrieve comments for the group."}
