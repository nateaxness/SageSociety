from pydantic import BaseModel
from typing import Union, Optional, List
from queries.pool import pool


class Error(BaseModel):
    message: str


class GroupIn(BaseModel):
    groups_name: str
    description: Optional[str]


class GroupOut(BaseModel):
    groups_id: int
    groups_name: str
    description: Optional[str]


class GroupRepository:
    def create_group(self, groups_data: GroupIn) -> Union[GroupOut, Error]:
        query = """
            INSERT INTO groups (groups_name, description)
            VALUES (%s, %s)
            RETURNING groups_id, groups_name, description;
        """
        try:
            with pool.connection() as conn:
                with conn.cursor() as cursor:
                    cursor.execute(
                        query,
                        (groups_data.groups_name, groups_data.description),
                    )
                    columns = ["groups_id", "groups_name", "description"]
                    row = cursor.fetchone()
                    row_dict = dict(zip(columns, row))
                    return GroupOut(**row_dict)

        except Exception as e:
            print(e)
            return {"message": "Failed to create group."}

    def get_group(self, groups_id: int) -> Union[GroupOut, Error]:
        query = """
            SELECT groups_id, groups_name, description
            FROM groups
            WHERE groups_id = %s;
        """
        try:
            with pool.connection() as conn:
                with conn.cursor() as cursor:
                    cursor.execute(query, (groups_id,))
                    columns = ["groups_id", "groups_name", "description"]
                    row = cursor.fetchone()
                    if not row:
                        return {"message": "Group not found."}
                    row_dict = dict(zip(columns, row))
                    return GroupOut(**row_dict)
        except Exception as e:
            print(e)
            return {"message": "Failed to retrieve group."}

    def list_all_groups(self) -> Union[List[GroupOut], Error]:
        query = """
            SELECT groups_id, groups_name, description
            FROM groups;
        """
        try:
            with pool.connection() as conn:
                with conn.cursor() as cursor:
                    cursor.execute(query)
                    columns = ["groups_id", "groups_name", "description"]
                    rows = cursor.fetchall()
                    return [
                        GroupOut(**dict(zip(columns, row))) for row in rows
                    ]

        except Exception as e:
            print(e)
            return {"message": "Failed to retrieve groups."}

    def delete_group(self, groups_id: int) -> Union[dict, Error]:
        query = """
            DELETE FROM groups
            WHERE groups_id = %s;
        """
        try:
            with pool.connection() as conn:
                with conn.cursor() as cursor:
                    cursor.execute(query, (groups_id,))
                    if cursor.rowcount == 0:
                        return {"message": "Group not found."}
                    return {"message": "Group successfully deleted."}

        except Exception as e:
            print(e)
            return {"message": "Failed to delete group."}

    def get_group_by_name(self, group_name: str) -> Union[GroupOut, Error]:
        query = """
            SELECT groups_id, groups_name, description
            FROM groups
            WHERE groups_name = %s;
        """
        try:
            with pool.connection() as conn:
                with conn.cursor() as cursor:
                    cursor.execute(query, (group_name,))
                    columns = ["groups_id", "groups_name", "description"]
                    row = cursor.fetchone()
                    if not row:
                        return {"message": "Group not found."}
                    row_dict = dict(zip(columns, row))
                    return GroupOut(**row_dict)
        except Exception as e:
            print(e)
            return {"message": "Failed to retrieve group."}
