from queries.pool import pool
from typing import Union, List
from pydantic import BaseModel


class Error(BaseModel):
    message: str


class BlogIn(BaseModel):
    accounts_id: int
    title: str


class BlogOut(BaseModel):
    blogs_id: int
    accounts_id: int
    title: str


class BlogListOut(BaseModel):
    blogs: List[BlogOut]


class BlogDeleteResponse(BaseModel):
    blogs_id: int
    message: str


class BlogRepository(BaseModel):
    def blog_in_to_out(self, blogs_id: int, blogs: BlogIn) -> BlogOut:
        old_data = blogs.dict()
        return BlogOut(blogs_id=blogs_id, **old_data)

    def record_to_blog_out(self, record) -> BlogOut:
        return BlogOut(
            blogs_id=record[0],
            accounts_id=record[1],
            title=record[2],
        )

    def create(self, blog: BlogIn) -> Union[BlogOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO blogs
                            (accounts_id, title)
                        VALUES
                            (%s, %s)
                        RETURNING blogs_id;
                        """,
                        [blog.accounts_id, blog.title],
                    )
                    id = result.fetchone()[0]
                    return self.blog_in_to_out(id, blog)
        except Exception as e:
            print(e)
            return Error(message="Could not create new blog")

    def get_blogs(self) -> Union[BlogListOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT blogs_id,
                               accounts_id,
                               title
                        FROM blogs
                        ORDER BY accounts_id
                        """
                    )
                    return BlogListOut(
                        blogs=[
                            self.record_to_blog_out(record)
                            for record in result
                        ]
                    )
        except Exception:
            return Error(message="Could not get all blogs")

    def get_one_blog(self, blogs_id: int) -> Union[BlogOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT blogs_id,
                               accounts_id,
                               title
                        FROM blogs
                        WHERE blogs_id = %s
                        """,
                        [blogs_id],
                    )
                    record = result.fetchone()
                    if record:
                        return self.record_to_blog_out(record)
                    else:
                        return Error(message="Blog not found")
        except Exception as e:
            print(e)
            return Error(message="Could not get blog")

    def delete(self, blogs_id: int) -> Union[BlogDeleteResponse, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE from blogs
                        WHERE blogs_id = %s
                        """,
                        [blogs_id],
                    )
                    return BlogDeleteResponse(
                        blogs_id=blogs_id,
                        message="Blog entry deleted successfully.",
                    )
        except Exception as e:
            print(e)
            return Error(message="Could not delete the blog")

    def update(
        self, blogs_id: int, updated_blog: BlogIn
    ) -> Union[BlogOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE blogs
                        SET
                            title = %s
                        WHERE blogs_id = %s
                        """,
                        [updated_blog.title, blogs_id],
                    )
                    return BlogOut(
                        blogs_id=blogs_id,
                        accounts_id=updated_blog.accounts_id,
                        title=updated_blog.title,
                    )
        except Exception as e:
            print(e)
            return Error(message="Could not update blog")
