from pydantic import BaseModel
from typing import List, Optional, Union
from datetime import date
from queries.pool import pool


class Error(BaseModel):
    message: str


class PlantIn(BaseModel):
    plants_name: str
    planted_date: date
    description: str
    gardens_id: int


class PlantOut(BaseModel):
    plants_id: int
    plants_name: str
    planted_date: date
    description: str
    gardens_id: int


class PlantRepository:
    def get_one(self, plants_id: int, gardens_id: int) -> Optional[PlantOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT plants_id, plants_name, planted_date, description, gardens_id, image_data
                        FROM plants WHERE plants_id = %s AND gardens_id = %s
                        """,
                        (plants_id, gardens_id),
                    )
                    record = db.fetchone()
                    print("DB Record:", record)
                    if record is None:
                        return None
                    return self.record_to_plant_out(record)
        except Exception as e:
            print(e)
            return {"message": "Could not get that plant"}

    def delete(self, plants_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                    DELETE FROM plants
                    where plants_id =%s
                    """,
                        [plants_id],
                    )
                    return True
        except Exception as e:
            print(e)
            return False

    def update(
        self, plants_id: int, plant: PlantIn, image_data: bytes
    ) -> Union[PlantOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE plants
                        SET plants_name = %s
                        , planted_date = %s
                        , description = %s
                        , image_data = %s
                        WHERE plants_id = %s
                        """,
                        [
                            plant.plants_name,
                            plant.planted_date,
                            plant.description,
                            image_data,
                            plants_id,
                        ],
                    )

                    db.execute(
                        "SELECT * FROM plants WHERE plants_id = %s",
                        [plants_id],
                    )
                    updated_plant = db.fetchone()
                    return PlantOut(
                        plants_id=updated_plant[0],
                        plants_name=updated_plant[1],
                        planted_date=updated_plant[2],
                        description=updated_plant[3],
                        gardens_id=updated_plant[4],
                    )
        except Exception as e:
            print(e)
            return Error(message="Failed to update the plant")

    def get_all(self, gardens_id: int) -> Union[Error, List[PlantOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT plants_id, plants_name, planted_date, description, gardens_id
                        FROM plants
                        WHERE gardens_id = %s
                        ORDER BY planted_date;
                        """,
                        [gardens_id],
                    )
                    result = []
                    for record in db:
                        plant = PlantOut(
                            plants_id=record[0],
                            plants_name=record[1],
                            planted_date=record[2],
                            description=record[3],
                            gardens_id=record[4],
                        )
                        result.append(plant)
                    return result
        except Exception as e:
            print(e)
            return {"message": "Could not get all plants"}

    def create(
        self, plant: PlantIn, gardens_id: int, image_data: bytes
    ) -> PlantOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    INSERT INTO plants
                        (plants_name, planted_date, description, gardens_id, image_data)
                    VALUES
                        (%s, %s, %s, %s, %s)
                    RETURNING plants_id;
                    """,
                    [
                        plant.plants_name,
                        plant.planted_date,
                        plant.description,
                        gardens_id,
                        image_data,
                    ],
                )
                id = db.fetchone()[0]
                old_data = plant.dict()
                old_data["plants_id"] = id
                return PlantOut(**old_data)

    def get_image_data(self, plants_id: int) -> bytes:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT image_data
                        FROM plants
                        WHERE plants_id = %s
                        """,
                        [plants_id],
                    )
                    record = result.fetchone()
                    if record is None:
                        raise Exception(
                            "No image data found for the given plant_id"
                        )
                    return record[0]
        except Exception as e:
            print(e)
            raise Exception("Failed to fetch image data")

    def plant_in_to_out(self, plants_id: int, plant: PlantIn):
        old_data = plant.dict()
        return PlantOut(id=plants_id, **old_data)

    def record_to_plant_out(self, record):
        return PlantOut(
            plants_id=record[0],
            plants_name=record[1],
            planted_date=record[2],
            description=record[3],
            gardens_id=record[4],
            image_data=record[5],
        )
