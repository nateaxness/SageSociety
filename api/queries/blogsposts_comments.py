from queries.pool import pool
from typing import Union, List
from pydantic import BaseModel


class Error(BaseModel):
    message: str


class BlogPostCommentDeleteResponse(BaseModel):
    blogs_post_comment_id: int
    message: str


class BlogPostCommentIn(BaseModel):
    accounts_id: int
    blogs_post_id: int
    text: str


class BlogPostCommentOut(BaseModel):
    blogs_post_comment_id: int
    accounts_id: int
    blogs_post_id: int
    text: str


class CommentList(BaseModel):
    comments: List[BlogPostCommentOut]


class BlogPostCommentRepository(BaseModel):
    def blogpost_comment_in_to_out(
        self, blogs_post_comment_id: int, blogpostcomment: BlogPostCommentIn
    ):
        old_data = blogpostcomment.dict()
        return BlogPostCommentOut(
            blogs_post_comment_id=blogs_post_comment_id, **old_data
        )

    def record_to_blogpost_comment_out(self, record):
        return BlogPostCommentOut(
            blogs_post_comment_id=record[0],
            accounts_id=record[1],
            blogs_post_id=record[2],
            text=record[3],
        )

    def create(
        self, blogpostcomment: BlogPostCommentIn, accounts_id: int
    ) -> BlogPostCommentOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                INSERT INTO blogs_post_comment
                    (
                    accounts_id,
                    blogs_post_id,
                    text
                    )
                VALUES
                    (%s, %s, %s)
                RETURNING blogs_post_comment_id;
                """,
                        [
                            accounts_id,
                            blogpostcomment.blogs_post_id,
                            blogpostcomment.text,
                        ],
                    )
                    id = result.fetchone()[0]
                    return self.blogpost_comment_in_to_out(id, blogpostcomment)
        except Exception as e:
            print(e)
            return {"message": "Could not create new Comment"}

    def get_all_comments(self, blogs_post_id) -> Union[CommentList, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                SELECT
                       blogs_post_comment_id,
                       accounts_id,
                       blogs_post_id,
                       text
                FROM blogs_post_comment
                WHERE blogs_post_id= %s
                ORDER BY blogs_post_comment_id
                """,
                        [blogs_post_id],
                    )
                    return [
                        self.record_to_blogpost_comment_out(record)
                        for record in result
                    ]
        except Exception:
            return {"message": "Could not get all comments"}

    def get_one_comment(self, blogspost_comment_id: int) -> BlogPostCommentOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                  SELECT blogs_post_comment_id,
                         accounts_id,
                         blogs_post_id,
                         text

                  FROM blogs_post_comment
                  WHERE blogs_post_comment_id = %s
                  """,
                        [blogspost_comment_id],
                    )
                record = db.fetchone()
                return self.record_to_blogpost_comment_out(record)
        except Exception as e:
            print(e)
            return {"message": "Could not get comment"}

    def delete(
        self, blogspost_comment_id: int
    ) -> Union[BlogPostCommentDeleteResponse, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                DELETE from blogs_post_comment
                WHERE blogs_post_comment_id = %s
                """,
                        [blogspost_comment_id],
                    )
                    return BlogPostCommentDeleteResponse(
                        blogs_post_comment_id=blogspost_comment_id,
                        message="Comment was succesfully deleted",
                    )

        except Exception as e:
            print(e)
            return Error(message="Could not delete comment")

    def update(
        self, blogspost_comment_id: int, updatedcomment: BlogPostCommentOut
    ) -> Union[BlogPostCommentOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                    UPDATE blogs_post_comment
                    SET
                        accounts_id = %s,
                        blogs_post_id= %s,
                        text = %s,
                    WHERE blogs_post_comment_id = %s
                    """,
                        [
                            updatedcomment.accounts_id,
                            updatedcomment.blogs_post_id,
                            updatedcomment.text,
                            updatedcomment.blogs_post_comment_id,
                        ],
                    )

                    updatedcomment.blogs_post_comment_id = blogspost_comment_id
                    return updatedcomment

        except Exception as e:
            print(e)
            return {"message": "Could not update comment"}
