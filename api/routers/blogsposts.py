from fastapi import APIRouter, Depends
from typing import Union
from queries.blogsposts import (
    BlogPostIn,
    BlogPostOut,
    BlogPostRepository,
    BlogPostList,
    Error,
)

from authenticator import authenticator


router = APIRouter()


@router.post(
    "/blog/{blogs_id}/blogpost",
    response_model=Union[BlogPostOut, Error],
    tags=["BlogPosts"],
)
def create_blog_post(
    blogpost: BlogPostIn,
    blogs_id: int,
    repo: BlogPostRepository = Depends(),
    current_account=Depends(authenticator.get_current_account_data),
):
    return repo.create(blogpost, blogs_id)


@router.get(
    "/blog/{blogs_id}/blogpost",
    response_model=Union[BlogPostList, Error],
    tags=["BlogPosts"],
)
def get_blog_posts_all(
    blogs_id: int,
    repo: BlogPostRepository = Depends(),
    current_account=Depends(authenticator.get_current_account_data),
) -> BlogPostList:
    result = repo.get_all_blogposts(blogs_id)
    # print("router result", result)
    new_result = BlogPostList(blogposts=result)
    return new_result


@router.get(
    "/blog/{blogs_id}/blogpost/{blogs_post_id}",
    response_model=Union[Error, BlogPostOut],
    tags=["BlogPosts"],
)
def get_detail_blog_post(
    blogs_post_id: int,
    repo: BlogPostRepository = Depends(),
    current_account=Depends(authenticator.get_current_account_data),
) -> BlogPostOut:
    return repo.get_one_blogpost(blogs_post_id)


@router.delete(
    "/blog/{blogs_id}/blogpost/{blogs_post_id}",
    response_model=Union[BlogPostOut, Error],
    tags=["BlogPosts"],
)
def delete_blog_post(
    blogs_post_id: int,
    repo: BlogPostRepository = Depends(),
    current_account=Depends(authenticator.get_current_account_data),
) -> bool:
    return repo.delete(blogs_post_id)


@router.put(
    "/blog/{blogs_id}/blogpost/{blogs_post_id}",
    response_model=Union[Error, BlogPostOut],
    tags=["BlogPosts"],
)
def update_blog_post(
    blogs_post_id: int,
    updated_blog_post: BlogPostOut,
    repo: BlogPostRepository = Depends(),
    current_account=Depends(authenticator.get_current_account_data),
) -> BlogPostOut:
    return repo.update(blogs_post_id, updated_blog_post)
