from fastapi import (
    APIRouter,
    Depends,
    Response,
    Request,
    status,
    HTTPException,
)
from typing import List, Optional
from queries.accounts import (
    AccountIn,
    AccountOut,
    AccountRepository,
    AccountUpdate,
    DuplicateAccountError,
)
from jwtdown_fastapi.authentication import Token
from authenticator import authenticator
from pydantic import BaseModel


class AccountForm(BaseModel):
    username: str
    password: str


class AccountToken(Token):
    account: AccountOut


class HttpError(BaseModel):
    detail: str


router = APIRouter()


@router.post("/api/accounts", response_model=AccountToken | HttpError)
async def create_account(
    info: AccountIn,
    request: Request,
    response: Response,
    repo: AccountRepository = Depends(),
):
    hashed_password = authenticator.hash_password(info.password)
    print("Hashed Password:", hashed_password)
    try:
        account = repo.create_account(info, hashed_password)
    except DuplicateAccountError:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Cannot create an account with those credentials",
        )
    form = AccountForm(username=info.username, password=info.password)
    token = await authenticator.login(response, request, form, repo)
    return AccountToken(account=account, **token.dict())


@router.get("/token", response_model=AccountToken | None)
async def retrieve_token(
    request: Request,
    account: AccountOut = Depends(authenticator.try_get_current_account_data),
) -> AccountToken | None:
    if account and authenticator.cookie_name in request.cookies:
        return {
            "access_token": request.cookies[authenticator.cookie_name],
            "type": "Bearer",
            "account": account,
        }


@router.get("/api/accounts", response_model=List[AccountOut])
def retrieve_all_accounts(repo: AccountRepository = Depends()):
    return repo.get_all()


@router.put("/api/accounts/{account_id}", response_model=AccountOut)
def modify_account(
    account_id: int,
    account_data: AccountUpdate,
    repo: AccountRepository = Depends(),
    current_account_data: dict = Depends(
        authenticator.get_current_account_data
    ),
):
    hashed_password = (
        authenticator.hash_password(account_data.password)
        if account_data.password
        else None
    )  # added password hash
    updated_account = repo.update_account(
        account_id, account_data, hashed_password
    )  # added "hashed_password" as an argument
    if updated_account is None:
        raise HTTPException(
            status_code=404,
            detail=f"Could not find an account with ID {account_id} to update",  # Adjusted error message
        )
    return updated_account


@router.delete("/api/accounts/{id}", response_model=bool)
def remove_account(
    id: int,
    repo: AccountRepository = Depends(),
    current_account_data: dict = Depends(
        authenticator.get_current_account_data
    ),
):
    result = repo.delete_account(id)
    if isinstance(result, dict) and "Error" in result:
        raise HTTPException(
            status_code=404,
            detail=result["Error"],
        )
    return True


@router.get(
    "/api/accounts/{username_or_id}", response_model=Optional[AccountOut]
)
def retrieve_account_by_username_or_id(
    username_or_id: str,
    response: Response,
    repo: AccountRepository = Depends(),
) -> Optional[AccountOut]:
    if username_or_id.isdigit():
        account = repo.get_account_by_id(int(username_or_id))
    else:
        account = repo.get_account(username_or_id)
    if account is None:
        response.status_code = 400
    return account
