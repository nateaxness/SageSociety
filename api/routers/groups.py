from fastapi import APIRouter, HTTPException, Depends
from queries.groups import GroupIn, GroupOut, GroupRepository, Error
from queries.accounts import AccountOut
from authenticator import authenticator
from typing import Union, List

router = APIRouter()
group_repository = GroupRepository()


@router.post(
    "/groups",
    response_model=Union[GroupOut, Error],
    status_code=201,
    tags=["groups"],
)
def create_group(
    group: GroupIn,
    _: AccountOut = Depends(authenticator.get_current_account_data),
):
    result = group_repository.create_group(group)
    if isinstance(result, dict) and "message" in result:
        raise HTTPException(status_code=400, detail=result["message"])
    return result


@router.get(
    "/groups/{groups_id}",
    response_model=Union[GroupOut, Error],
    tags=["groups"],
)
def get_group(
    groups_id: int,
    _: AccountOut = Depends(authenticator.get_current_account_data),
):
    # Authentication is enforced. If not authenticated, it won't proceed to this function.

    result = group_repository.get_group(groups_id)
    if isinstance(result, dict) and "message" in result:
        raise HTTPException(status_code=404, detail=result["message"])
    return result


@router.get(
    "/groups",
    response_model=List[GroupOut],
    tags=["groups"],
)
def list_groups(
    _: AccountOut = Depends(authenticator.get_current_account_data),
):
    results = group_repository.list_all_groups()
    if isinstance(results, Error):
        raise HTTPException(status_code=404, detail=results.message)
    return results


@router.delete(
    "/groups/{groups_id}",
    response_model=Union[dict, Error],
    tags=["groups"],
)
def delete_group(
    groups_id: int,
    _: AccountOut = Depends(authenticator.get_current_account_data),
):
    result = group_repository.delete_group(groups_id)
    if "message" in result and result["message"] == "Group not found.":
        raise HTTPException(status_code=404, detail=result["message"])
    elif (
        "message" in result and result["message"] == "Failed to delete group."
    ):
        raise HTTPException(status_code=400, detail=result["message"])
    return result


@router.get(
    "/groups/name/{group_name}",
    response_model=Union[GroupOut, Error],
    tags=["groups"],
)
def get_group_by_name(
    group_name: str,
    _: AccountOut = Depends(authenticator.get_current_account_data),
):
    result = group_repository.get_group_by_name(group_name)
    if isinstance(result, dict) and "message" in result:
        raise HTTPException(status_code=404, detail=result["message"])
    return result
