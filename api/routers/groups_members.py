from fastapi import APIRouter, HTTPException, Body, Depends
from typing import List
from queries.groups_members import (
    GroupMemberRepository,
    GroupMemberIn,
    GroupMemberOut,
    Error,
)
from queries.accounts import AccountOut
from authenticator import authenticator

router = APIRouter()
group_member_repository = GroupMemberRepository()


@router.post(
    "/groups-members",
    response_model=GroupMemberOut,
    tags=["GroupMembers"],
    operation_id="add_group_member",
    status_code=201,
)
def add_group_member(
    member_data: GroupMemberIn = Body(...),
    _: AccountOut = Depends(authenticator.get_current_account_data),
):
    # Authentication is enforced. If not get_current_account_datad, it won't proceed to this function.

    result = group_member_repository.add_member(member_data)

    if isinstance(result, Error):
        raise HTTPException(status_code=400, detail=result.message)

    return result


@router.get(
    "/groups-members/{groups_id}",
    response_model=List[GroupMemberOut],
    tags=["GroupMembers"],
    operation_id="get_group_members",
)
def get_group_members(
    groups_id: int,
    _: AccountOut = Depends(authenticator.get_current_account_data),
):
    # Authentication is enforced. If not get_current_account_datad, it won't proceed to this function.

    results = group_member_repository.get_members(groups_id)

    if isinstance(results, Error):
        raise HTTPException(status_code=404, detail=results.message)

    return results
