from fastapi import APIRouter, Depends
from typing import Union
from queries.blogsposts_comments import (
    BlogPostCommentIn,
    BlogPostCommentOut,
    BlogPostCommentRepository,
    CommentList,
    Error,
)

from authenticator import authenticator


router = APIRouter()


@router.post(
    "/blog/{blogs_id}/blogpost/{blogs_post_id}/comment",
    response_model=Union[BlogPostCommentOut, Error],
    tags=["BlogPost Comments"],
)
def create_comment(
    blogpostcomment: BlogPostCommentIn,
    current_account=Depends(authenticator.get_current_account_data),
    repo: BlogPostCommentRepository = Depends(),
):
    return repo.create(blogpostcomment, current_account["id"])


@router.get(
    "/blog/{blogs_id}/blogpost/{blogs_post_id}/comment",
    response_model=Union[CommentList, Error],
    tags=["BlogPost Comments"],
)
def get_all_comments(
    blogs_post_id: int,
    current_account=Depends(authenticator.get_current_account_data),
    repo: BlogPostCommentRepository = Depends(),
) -> CommentList:
    result = repo.get_all_comments(blogs_post_id)
    return CommentList(comments=result)


@router.get(
    "/blog/{blogs_id}/blogpost/{blogs_post_id}/comment/{blogs_post_comment_id}",
    response_model=Union[Error, BlogPostCommentOut],
    tags=["BlogPost Comments"],
)
def get_one_comment(
    blogs_post_comment_id: int,
    current_account=Depends(authenticator.get_current_account_data),
    repo: BlogPostCommentRepository = Depends(),
) -> BlogPostCommentOut:
    return repo.get_one_comment(blogs_post_comment_id)


@router.delete(
    "/blog/{blogs_id}/blogpost/{blogs_post_id}/comment/{blogs_post_comment_id}",
    response_model=Union[BlogPostCommentOut, Error],
    tags=["BlogPost Comments"],
)
def delete_comment(
    blogs_post_comment_id: int,
    current_account=Depends(authenticator.get_current_account_data),
    repo: BlogPostCommentRepository = Depends(),
) -> bool:
    return repo.delete(blogs_post_comment_id)


@router.put(
    "/blog/{blogs_id}/blogpost/{blogs_post_id}/comment/{blogs_post_comment_id}",
    response_model=Union[Error, BlogPostCommentOut],
    tags=["BlogPost Comments"],
)
def update_comment(
    blogs_post_comment_id: int,
    updatedcomment: BlogPostCommentOut,
    current_account=Depends(authenticator.get_current_account_data),
    repo: BlogPostCommentRepository = Depends(),
) -> BlogPostCommentOut:
    return repo.update(blogs_post_comment_id, updatedcomment)
