from fastapi import APIRouter, Depends
from typing import Union
from queries.blogs import BlogIn, BlogOut, BlogRepository, Error, BlogListOut

from authenticator import authenticator

router = APIRouter()


@router.post("/blog", response_model=Union[BlogOut, Error], tags=["Blogs"])
def create_blog(
    blogs: BlogIn,
    current_account=Depends(authenticator.get_current_account_data),
    repo: BlogRepository = Depends(),
):
    return repo.create(blogs)


@router.get("/blog", response_model=Union[BlogListOut, Error], tags=["Blogs"])
def get_blogs(
    repo: BlogRepository = Depends(),
    current_account=Depends(authenticator.get_current_account_data),
):
    return repo.get_blogs()


@router.get(
    "/blog/{blogs_id}", response_model=Union[Error, BlogOut], tags=["Blogs"]
)
def get_detail_blog(
    blogs_id: int,
    current_account=Depends(authenticator.get_current_account_data),
    repo: BlogRepository = Depends(),
) -> BlogOut:
    return repo.get_one_blog(blogs_id)


@router.delete(
    "/blog/{blogs_id}", response_model=Union[BlogOut, Error], tags=["Blogs"]
)
def delete_blog(
    blogs_id: int,
    repo: BlogRepository = Depends(),
    current_account=Depends(authenticator.get_current_account_data),
) -> bool:
    return repo.delete(blogs_id)


@router.put(
    "/blog/{blogs_id}", response_model=Union[Error, BlogOut], tags=["Blogs"]
)
def update_blog(
    blogs_id: int,
    updated_blog: BlogIn,
    current_account=Depends(authenticator.get_current_account_data),
    repo: BlogRepository = Depends(),
) -> BlogOut:
    return repo.update(blogs_id, updated_blog)
