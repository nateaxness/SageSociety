from fastapi import APIRouter, Depends, HTTPException
from queries.gardens import (
    GardenQueries,
    GardenListOut,
    GardenOut,
    GardenIn,
    GardenUpdate,
)
from queries.accounts import AccountOut
from authenticator import authenticator

router = APIRouter()


@router.get("/api/gardens", response_model=GardenListOut, tags=["gardens"])
def get_gardens(
    current_account: dict = Depends(authenticator.get_current_account_data),
    queries: GardenQueries = Depends(),
):
    accounts_id = current_account["id"]
    return {"gardens": queries.get_all_gardens(accounts_id)}


# GET by Gardens ID
@router.get(
    "/api/gardens/{gardens_id}", response_model=GardenOut, tags=["gardens"]
)
def get_garden_by_id(
    gardens_id: int,
    current_account: AccountOut = Depends(
        authenticator.get_current_account_data
    ),
    queries: GardenQueries = Depends(),
):
    accounts_id = current_account["id"]
    garden = queries.get_garden(gardens_id, accounts_id)
    if garden is None:
        raise HTTPException(
            status_code=404,
            detail="Garden not found",
        )
    return garden


@router.post("/api/gardens", response_model=GardenOut, tags=["gardens"])
def create_garden(
    garden: GardenIn,
    _: AccountOut = Depends(authenticator.get_current_account_data),
    queries: GardenQueries = Depends(),
):
    return queries.create_garden(garden)


@router.delete(
    "/api/gardens/{gardens_id}", response_model=bool, tags=["gardens"]
)
def delete_garden(
    gardens_id: int,
    current_account: AccountOut = Depends(
        authenticator.get_current_account_data
    ),
    queries: GardenQueries = Depends(),
):
    accounts_id = current_account["id"]
    queries.delete_garden(gardens_id, accounts_id)
    return True


@router.put(
    "/api/gardens/{gardens_id}", response_model=GardenOut, tags=["gardens"]
)
def update_garden(
    gardens_id: int,
    garden: GardenUpdate,
    current_account: AccountOut = Depends(
        authenticator.get_current_account_data
    ),
    queries: GardenQueries = Depends(),
):
    accounts_id = current_account["id"]
    updated_garden = queries.update_garden(gardens_id, accounts_id, garden)
    if updated_garden is None:
        raise HTTPException(
            status_code=404,
            detail="Couldn't find garden to update",
        )
    return updated_garden
