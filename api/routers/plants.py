from fastapi import (
    APIRouter,
    Depends,
    HTTPException,
    UploadFile,
    File,
    Request,
    Query,
)
from queries.plants import PlantRepository, PlantIn, PlantOut, Error
from typing import Union, List, Optional
from datetime import date
from fastapi.responses import StreamingResponse
import io
import requests
from authenticator import authenticator
from datetime import datetime

router = APIRouter()


@router.post("/getCareInstructions/{plants_name}/{zip_code}")
def get_care_instructions(
    request: Request, plants_name: str = Query(...), zip_code: str = Query(...)
):
    OPENAI_API_KEY = "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"
    headers = {
        "Authorization": f"Bearer {OPENAI_API_KEY}",
        "Content-Type": "application/json",
    }

    current_month = datetime.now().month
    if 3 <= current_month <= 5:
        season = "spring"
    elif 6 <= current_month <= 8:
        season = "summer"
    elif 9 <= current_month <= 11:
        season = "autumn"
    else:
        season = "winter"

    data = {
        "model": "gpt-3.5-turbo",
        "messages": [
            {
                "role": "user",
                # eslint-disable-next-line react-hooks/exhaustive-deps
                "content": f"Provide plant or tree care instructions for {plants_name} in zip code {zip_code} during this season's climate during the {season} season. When answering the question refer to the city name and never the zip code number as this will be displayed on our website's frontend.",
            }
        ],
    }

    url = "https://api.openai.com/v1/chat/completions"

    response = requests.post(url, headers=headers, json=data)
    print(response.text)
    if response.status_code != 200:
        raise HTTPException(
            status_code=response.status_code,
            detail="Failed to fetch from OpenAI API",
        )

    return response.json()


@router.get("/fetch_plant/{plants_name}")
def fetch_plant(plants_name: str):
    TREFLE_API_KEY = "05hbLzpM1jpaUtB7APT3-vjfDGNNBdEiAj9nTQwnTyY"
    url = f"https://trefle.io/api/v1/plants/search?token={TREFLE_API_KEY}&q={plants_name}"

    response = requests.get(url)
    if response.status_code != 200:
        raise HTTPException(
            status_code=response.status_code,
            detail="Failed to fetch from Trefle API",
        )

    return response.json()


@router.get("/gardens/{gardens_id}/plants/{plant_id}/image/")
async def get_plant_image(plant_id: int, repo: PlantRepository = Depends()):
    image_data = repo.get_image_data(plant_id)
    return StreamingResponse(io.BytesIO(image_data), media_type="image/jpeg")


@router.post(
    "/gardens/{gardens_id}/plants/",
    response_model=Union[PlantOut, Error],
    tags=["plants"],
)
async def create_plant(
    gardens_id: int,
    plants_name: str,
    planted_date: date,
    description: str,
    image_file: UploadFile = File(...),
    repo: PlantRepository = Depends(),
):
    try:
        plant = PlantIn(
            plants_name=plants_name,
            planted_date=planted_date,
            description=description,
            gardens_id=gardens_id,
        )

        image_data = await convert_image_to_bytes(image_file)

        new_plant = repo.create(plant, gardens_id, image_data)
        return new_plant

    except Exception as e:
        return Error(message=f"Failed to create the plant: {str(e)}")


async def convert_image_to_bytes(image_file: UploadFile):
    try:
        image_data = await image_file.read()
        return image_data
    except Exception as e:
        raise HTTPException(
            status_code=500, detail=f"Image conversion failed: {str(e)}"
        )


@router.get(
    "/gardens/{gardens_id}/plants/",
    response_model=Union[Error, List[PlantOut]],
    tags=["plants"],
)
def get_all_plants(
    gardens_id: int,
    current_account=Depends(authenticator.get_current_account_data),
    repo: PlantRepository = Depends(),
):
    plants = repo.get_all(gardens_id)
    if isinstance(plants, Error):
        raise HTTPException(status_code=500, detail=plants.message)
    return plants


@router.put(
    "/gardens/{gardens_id}/plants/{plants_id}/",
    response_model=Union[PlantOut, Error],
    tags=["plants"],
)
async def update_plant(
    plants_id: int,
    gardens_id: int,
    plants_name: str,
    planted_date: date,
    description: str,
    image_file: Optional[UploadFile] = File(...),
    repo: PlantRepository = Depends(),
):
    try:
        plant = PlantIn(
            plants_name=plants_name,
            planted_date=planted_date,
            description=description,
            gardens_id=gardens_id,
        )

        if image_file:
            image_data = await convert_image_to_bytes(image_file)
        else:
            image_data = None

        updated_plant = repo.update(plants_id, plant, image_data)
        return updated_plant
    except Exception as e:
        return Error(message=f"Failed to update the plant: {str(e)}")


@router.delete(
    "/gardens/{gardens_id}/plants/{plants_id}/",
    response_model=bool,
    tags=["plants"],
)
def delete_plant(
    plants_id: int,
    current_account=Depends(authenticator.get_current_account_data),
    repo: PlantRepository = Depends(),
):
    if not repo.delete(plants_id):
        raise HTTPException(status_code=500, detail="Failed to delete plant")
    return True


@router.get(
    "/gardens/{gardens_id}/plants/{plants_id}/",
    response_model=Union[PlantOut, Error],
    tags=["plants"],
)
def get_one_plant(
    plants_id: int,
    gardens_id: int,
    current_account=Depends(authenticator.get_current_account_data),
    repo: PlantRepository = Depends(),
):
    plant = repo.get_one(plants_id=plants_id, gardens_id=gardens_id)
    if not plant:
        raise HTTPException(status_code=404, detail="Plant not found")
    return plant
